const puppeteer = require('puppeteer');
const Xvfb      = require('xvfb');
const fs = require('fs');
const os = require('os');
const homedir = os.homedir();
const platform = os.platform();
const config = JSON.parse(fs.readFileSync("config.json", 'utf8'));
const spawn = require('child_process').spawn;

const AWS = require('aws-sdk');


var xvfb        = new Xvfb({
    silent: true,
    xvfb_args: ["-screen", "0", "1280x800x24", "-ac", "-nolisten", "tcp", "-dpi", "96", "+extension", "RANDR"]
});
var width       = 1280;
var height      = 720;
var options     = {
  headless: false,
  args: [
    '--enable-usermedia-screen-capturing',
    '--allow-http-screen-capture',
    '--auto-select-desktop-capture-source=bbbrecorder',
    '--load-extension=' + __dirname,
    '--disable-extensions-except=' + __dirname,
    '--disable-infobars',
    '--no-sandbox',    
    '--shm-size=1gb',
    '--disable-dev-shm-usage',
    '--start-fullscreen',
    '--app=https://www.google.com/',
    `--window-size=${width},${height}`,
  ],
}

if(platform == "linux"){
    options.executablePath = "/usr/bin/google-chrome"
}else if(platform == "darwin"){
    options.executablePath = "/Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome"
}

async function main() {
    try{
        if(platform == "linux"){
            xvfb.startSync()
        }
        var url = process.argv[2],
            exportname = process.argv[3], 
            duration = process.argv[4],
            convert = process.argv[5],
            bucket = process.argv[6],
            key = process.argv[7],
            secret = process.argv[8]

        if(!url){ url = 'https://www.mynaparrot.com/' }
        if(!exportname){ exportname = 'live.webm' }
        //if(!duration){ duration = 10 }
        if(!convert){ convert = false }
        
        const browser = await puppeteer.launch(options)
        const pages = await browser.pages()
        
        const page = pages[0]

        page.on('console', msg => {
            var m = msg.text();
            //console.log('PAGE LOG:', m) // uncomment if you need
        });

        await page._client.send('Emulation.clearDeviceMetricsOverride')
        await page.goto(url, {waitUntil: 'networkidle2'})
        await page.setBypassCSP(true)

        await page.waitForSelector('[aria-label="Listen only"]');
        await page.click('[aria-label="Listen only"]', {waitUntil: 'domcontentloaded'});

        await page.waitForSelector('[id="chat-toggle-button"]');
        await page.click('[id="chat-toggle-button"]', {waitUntil: 'domcontentloaded'});
        await page.click('button[aria-label="Users and messages toggle"]', {waitUntil: 'domcontentloaded'});
        await page.$eval('[class^=navbar]', element => element.style.display = "none");

        await page.$eval('.Toastify', element => element.style.display = "none");
        await page.waitForSelector('button[aria-label="Leave audio"]');
        await page.$eval('[class^=actionsbar] > [class^=center]', element => element.style.display = "none");
        await page.mouse.move(0, 700);
        
        await page.evaluate((x) => {
            console.log("REC_START");
            window.postMessage({type: 'REC_START'}, '*')
        })

        if(duration > 0){
            await page.waitFor((duration * 1000))
        }else{
            await page.waitForSelector('[class^=modal] > [class^=content] > button[description="Logs you out of the meeting"]', {
                timeout: 0
            });
        }

        await page.evaluate(filename=>{
            window.postMessage({type: 'SET_EXPORT_PATH', filename: filename}, '*')
            window.postMessage({type: 'REC_STOP'}, '*')
        }, exportname)

        // Wait for download of webm to complete
        await page.waitForSelector('html.downloadComplete', {timeout: 0})
        await page.close()
        await browser.close()

        if(platform == "linux"){
            xvfb.stopSync()
        }

        if(convert=='True'){
            convertAndCopy(exportname,bucket,key,secret)

        }else{
            console.log("---------Desactivado guardar en formato webm--------")
        }
        
    }catch(err) {
        console.log(err)
    }
}

main()

function uploadFile (filename,bucket,key,secret) {

    var copyToPath = "/var/snap/amazon-ssm-agent/current/bbb-recorder/Downloads";
    var copyTo = copyToPath + "/" + filename + ".mp4";

    console.log("subiendo a AWS S3")
    const s3 = new AWS.S3({
    accessKeyId: key,
    secretAccessKey: secret
    });
    // Read content from the file
    const fileContent = fs.readFileSync(copyTo);

    // Setting up S3 upload parameters
    const params = {
        Bucket: bucket,
        Key: filename+ ".mp4", // File name you want to save as in S3
        Body: fileContent
    };

    // Uploading files to the bucket
    s3.upload(params, function(err, data) {
        if (err) {
            throw err;
        }
        console.log(`File uploaded successfully. ${data.Location}`);
    });
}

//uploadFile('liveRecord2.mp4');

function convertAndCopy(filename,bucket,key,secret){
 
    var copyFromPath = "/var/snap/amazon-ssm-agent/current/bbb-recorder/Downloads";

    var copyToPath = "/var/snap/amazon-ssm-agent/current/bbb-recorder/Downloads";

    var copyFrom = copyFromPath + "/" + filename + ".webm";
    var copyTo = copyToPath + "/" + filename + ".mp4";

    if(!fs.existsSync(copyToPath)){
        fs.mkdirSync(copyToPath);
    }

    console.log(copyTo);
    console.log(copyFrom);

    const ls = spawn('ffmpeg',
        [   '-y',
            '-i "' + copyFrom + '"',
            '-c:v libx264',
            '-preset veryfast',
            '-movflags faststart',
            '-profile:v high',
            '-level 4.2',
            '-max_muxing_queue_size 9999',
            '-vf mpdecimate',
            '-vsync vfr "' + copyTo + '"'
        ],
        {
            shell: true
        }

    );

    ls.stdout.on('data', (data) => {
        console.log(`stdout: ${data}`);
    });

    ls.stderr.on('data', (data) => {
        console.error(`stderr: ${data}`);
    });



    ls.on('close', (code) => {
        console.log(`child process exited with code ${code}`);
        if(code == 0)
        {
            console.log("Convertion done to here: " + copyTo)
            fs.unlinkSync(copyFrom);
            console.log('successfully deleted ' + copyFrom);
            uploadFile(filename,bucket,key,secret)
        }
       
    });


}


